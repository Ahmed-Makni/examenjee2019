package tn.esprit.service;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.entities.User;
@Remote
public interface IUserRemoteService {
	public int addUser(User user);

	public void removeUser(int id);

	public void updateUser(User user);

	public User findUserById(int id);

	public List<User> findAllUsers();
	public User getUserByEmailAndPassword(String login, String password); 
}
