package tn.esprit.service;

import javax.ejb.Remote;

import tn.esprit.entities.User;

@Remote
public interface ExempleRemote extends IRemoteService<User, Integer> {

}
