package tn.esprit.service;

import javax.ejb.Local;

import tn.esprit.entities.User;

@Local
public interface ExempleLocal extends ILocalService<User, Integer>{

}
