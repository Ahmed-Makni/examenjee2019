package tn.esprit.service.Impl;

import javax.ejb.Remote;

import tn.esprit.entities.Project;
import tn.esprit.service.IRemoteService;

@Remote
public interface IProjectServiceRemote extends IRemoteService<Project, Integer>{

}
