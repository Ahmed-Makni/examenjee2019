package tn.esprit.service.Impl;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import tn.esprit.entities.Spint;

/**
 * Session Bean implementation class SprintService
 */
@Stateless
@LocalBean
public class SprintService extends ServiceCRUD<Spint, Integer> implements ISprintRemote, ISprintLocal {

	public SprintService() {
		super(Spint.class, Integer.class);
		// TODO Auto-generated constructor stub
	}

    /**
     * Default constructor. 
     */
   

}
