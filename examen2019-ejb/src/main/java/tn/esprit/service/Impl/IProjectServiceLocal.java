package tn.esprit.service.Impl;

import javax.ejb.Local;

import tn.esprit.entities.Project;
import tn.esprit.service.ILocalService;

@Local
public interface IProjectServiceLocal extends ILocalService<Project, Integer>{

}
