package tn.esprit.service.Impl;

import javax.ejb.Remote;

import tn.esprit.entities.Spint;
import tn.esprit.service.IRemoteService;

@Remote
public interface ISprintRemote extends IRemoteService<Spint, Integer>{

}
