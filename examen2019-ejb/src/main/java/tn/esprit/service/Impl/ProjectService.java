package tn.esprit.service.Impl;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import tn.esprit.entities.Project;

/**
 * Session Bean implementation class ProjectService
 */
@Stateless
@LocalBean
public class ProjectService extends ServiceCRUD<Project, Integer> implements IProjectServiceRemote, IProjectServiceLocal {

	public ProjectService() {
		super(Project.class, Integer.class);
		// TODO Auto-generated constructor stub
	}

    /**
     * Default constructor. 
     */
    

}
