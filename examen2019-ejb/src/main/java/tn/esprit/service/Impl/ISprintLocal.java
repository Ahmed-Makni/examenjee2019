package tn.esprit.service.Impl;

import javax.ejb.Local;

import tn.esprit.entities.Project;
import tn.esprit.entities.Spint;
import tn.esprit.service.ILocalService;

@Local
public interface ISprintLocal extends ILocalService<Spint, Integer>{

}
