package tn.esprit.service.Impl;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.entities.User;
import tn.esprit.service.IUserLocalService;
import tn.esprit.service.IUserRemoteService;
@Stateless
@LocalBean
public class UserService implements IUserRemoteService, IUserLocalService {

	@PersistenceContext(unitName = "examen2019-ejb")
	EntityManager em;
	
	@Override
	public int addUser(User user) {
		// TODO Auto-generated method stub
		em.persist(user);
		return user.getId();
	}

	@Override
	public void removeUser(int id) {
		// TODO Auto-generated method stub
		User user=em.find(User.class, id);
		em.remove(user);
	}

	@Override
	public void updateUser(User user) {
		// TODO Auto-generated method stub
		em.merge(user);

	}

	@Override
	public User findUserById(int id) {
		// TODO Auto-generated method stub
		User user=em.find(User.class, id);
		return user;
	}

	@Override
	public List<User> findAllUsers() {
		// TODO Auto-generated method stub
		List<User> users = em.createQuery("from Question ", User.class).getResultList();
		return users;
	}

	@Override
	public User getUserByEmailAndPassword(String login, String password) {
		// TODO Auto-generated method stub
		TypedQuery<User> query = 
				em.createQuery("select u from User u WHERE u.nom=:nom and u.password=:password ", User.class); 
				query.setParameter("nom", login); 
				query.setParameter("password", password); 
				User user = null; 
				try {
					user = query.getSingleResult(); 
				}
				catch (Exception e) {
					System.out.println("Erreur : " + e);
				}
				return user;
	}

}
