package tn.esprit.service;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import tn.esprit.entities.User;
import tn.esprit.service.Impl.ServiceCRUD;

/**
 * Session Bean implementation class Exemple
 */
@Stateless
@LocalBean
public class Exemple extends ServiceCRUD<User, Integer> implements ExempleRemote, ExempleLocal {

	public Exemple() {
		super(User.class, Integer.class);
		// TODO Auto-generated constructor stub
	}

    /**
     * Default constructor. 
     */


}
