package tn.esprit.entities;

import java.io.Serializable;
import java.lang.String;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Projetct
 *
 */
@Entity
public class Project implements Serializable {

	   

	private int id;
	private String title;
	private String description;
	private Set<Spint> spints=new HashSet<>();
	private Set<User> users=new HashSet<>();
	private static final long serialVersionUID = 1L;

	public Project() {
		super();
	}   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.EAGER,mappedBy="projetct")
	public Set<Spint> getSpints() {
		return spints;
	}
	public void setSpints(Set<Spint> spints) {
		this.spints = spints;
	}
	public Project(int id, String title, String description) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
	
	}
	@ManyToMany(cascade=CascadeType.ALL,mappedBy="projects",fetch=FetchType.EAGER)
	public Set<User> getUsers() {
		return users;
	}
	public void setUsers(Set<User> users) {
		this.users = users;
	}
   
	
}
