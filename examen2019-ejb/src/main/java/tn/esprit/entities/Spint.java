package tn.esprit.entities;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Spint
 *
 */
@Entity

public class Spint implements Serializable {

	   

	private int id;
	private String description;
	private Date startDate;
	private Project project;
	private static final long serialVersionUID = 1L;

	public Spint() {
		super();
	}   
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}   
	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	@ManyToOne(cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	@JoinColumn(name="projectId", nullable = true)
	public Project getProjetct() {
		return project;
	}
	public void setProjetct(Project project) {
		this.project = project;
	}
	public Spint(int id, String description, Date startDate, Project project) {
		super();
		this.id = id;
		this.description = description;
		this.startDate = startDate;
		this.project = project;
	}
   
	
}
